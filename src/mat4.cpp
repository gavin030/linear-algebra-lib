#include "mat4.h"

mat4::mat4()
    : data({vec4(), vec4(), vec4(), vec4()})
{
}

mat4::mat4(float diag)
    : data({vec4(diag, 0, 0, 0), vec4(0, diag, 0, 0),
            vec4(0, 0, diag, 0), vec4(0, 0, 0, diag)})
{
}

mat4::mat4(const vec4& col0, const vec4& col1, const vec4& col2, const vec4& col3)
    : data({vec4(col0), vec4(col1), vec4(col2), vec4(col3)})
{
}

mat4::mat4(const mat4 &m2)
    : data({vec4(m2[0]), vec4(m2[1]), vec4(m2[2]), vec4(m2[3])})
{
}

vec4 mat4::operator[](unsigned int index) const
{
    if(index > 3 || index < 0) throw std::out_of_range("index must be in [0, 3]");
    return data[index];
}

vec4& mat4::operator[](unsigned int index)
{
    if(index > 3 || index < 0) throw std::out_of_range("index must be in [0, 3]");
    return data[index];
}

mat4 mat4::rotate(float angle, float x, float y, float z)
{
    float radian = angle * M_PI / 180;

    if(x == (float)1){
        return mat4(vec4(1, 0, 0, 0),
                    vec4(0, cos(radian), sin(radian), 0),
                    vec4(0, -sin(radian), cos(radian), 0),
                    vec4(0, 0, 0, 1));
    }else if(y == (float)1){
        return mat4(vec4(cos(radian), 0, -sin(radian), 0),
                    vec4(0, 1, 0, 0),
                    vec4(sin(radian), 0, cos(radian), 0),
                    vec4(0, 0, 0, 1));
    }else if(z == (float)1){
        return mat4(vec4(cos(radian), sin(radian), 0, 0),
                    vec4(-sin(radian), cos(radian), 0, 0),
                    vec4(0, 0, 1, 0),
                    vec4(0, 0, 0, 1));
    }else{
        throw std::invalid_argument("input doesn't correspond to standard");
    }
}

mat4 mat4::translate(float x, float y, float z)
{
    return mat4(vec4(1, 0, 0, 0),
                vec4(0, 1, 0, 0),
                vec4(0, 0, 1, 0),
                vec4(x, y, z, 1));
}

mat4 mat4::scale(float x, float y, float z)
{
    return mat4(vec4(x, 0, 0, 0),
                vec4(0, y, 0, 0),
                vec4(0, 0, z, 0),
                vec4(0, 0, 0, 1));
}

mat4 mat4::identity()
{
    return mat4(vec4(1, 0, 0, 0),
                vec4(0, 1, 0, 0),
                vec4(0, 0, 1, 0),
                vec4(0, 0, 0, 1));
}

mat4& mat4::operator=(const mat4 &m2)
{
    for(unsigned int i = 0; i <= 3; i++){
        data[i] = m2[i];
    }

    return *this;
}

bool mat4::operator==(const mat4 &m2) const
{
    return data[0] == m2[0] && data[1] == m2[1]
        && data[2] == m2[2] && data[3] == m2[3];
}

bool mat4::operator !=(const mat4 &m2) const
{
    return !(*this == m2);
}

mat4& mat4::operator+=(const mat4 &m2)
{
    *this = *this + m2;

    return *this;
}

mat4& mat4::operator-=(const mat4 &m2)
{
    *this = *this - m2;

    return *this;
}

mat4& mat4::operator*=(float c)
{
    *this = *this * c;

    return *this;
}

mat4& mat4::operator/=(float c)
{
    *this = *this / c;

    return *this;
}

mat4 mat4::operator+(const mat4 &m2) const
{
    return mat4(data[0]+m2[0], data[1]+m2[1],
                data[2]+m2[2], data[3]+m2[3]);
}

mat4 mat4::operator-(const mat4 &m2) const
{
    return mat4(data[0]-m2[0], data[1]-m2[1],
                data[2]-m2[2], data[3]-m2[3]);
}

mat4 mat4::operator*(float c) const
{
    return mat4(data[0]*c, data[1]*c, data[2]*c, data[3]*c);
}

mat4 mat4::operator/(float c) const
{
    if(fabs(c - 0) <= FLT_EPSILON) throw std::invalid_argument("divisor can't be 0");
    return mat4(data[0]/c, data[1]/c, data[2]/c, data[3]/c);
}

mat4 mat4::operator*(const mat4 &m2) const
{
    mat4 m3 = mat4();

    for(unsigned int i = 0; i <= 3; i++){
        for(unsigned int j = 0; j <= 3; j++){
            for(unsigned int k = 0; k <= 3; k++){
                m3[j][i] += data[k][i] * m2[j][k];
            }
        }
    }

    return m3;
}

vec4 mat4::operator*(const vec4 &v) const
{
    vec4 v2 = vec4();

    for(unsigned int i = 0; i <= 3; i++){
        for(unsigned int j = 0; j <= 3; j++){
            v2[i] += data[j][i] * v[j];
        }
    }

    return v2;
}

mat4 transpose(const mat4 &m)
{
    mat4 m2 = mat4();

    for(unsigned int i = 0; i <= 3; i++){
        for(unsigned int j = 0; j <= 3; j++){
            m2[i][j] = m[j][i];
        }
    }

    return m2;
}

vec4 row(const mat4 &m, unsigned int index)
{
    if(index > 3) throw std::out_of_range("index must be in [0, 3]");
    return vec4(m[0][index], m[1][index], m[2][index], m[3][index]);
}

mat4 operator*(float c, const mat4 &m)
{
    return m * c;
}

vec4 operator*(const vec4 &v, const mat4 &m)
{
    return vec4(v[0]*m[0][0], v[1]*m[0][0], v[2]*m[0][0], v[3]*m[0][0]);
}

std::ostream& operator<<(std::ostream& o, const mat4 &m){
    o<<"["<<m[0][0]<<" "<<m[1][0]<<" "<<m[2][0]<<" "<<m[3][0]<<"\n"
     <<" "<<m[0][1]<<" "<<m[1][1]<<" "<<m[2][1]<<" "<<m[3][1]<<"\n"
     <<" "<<m[0][2]<<" "<<m[1][2]<<" "<<m[2][2]<<" "<<m[3][2]<<"\n"
     <<" "<<m[0][3]<<" "<<m[1][3]<<" "<<m[2][3]<<" "<<m[3][3]<<"]";

    return o;
}
