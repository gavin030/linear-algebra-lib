#include "vec4.h"

vec4::vec4()
    : data({0})
{
}

vec4::vec4(float x, float y, float z, float w)
    : data({x, y, z, w})
{
}

vec4::vec4(const vec4 &v2)
    : data({v2[0], v2[1], v2[2], v2[3]})
{
}

float vec4::operator[](unsigned int index) const
{
    if(index > 3 || index < 0) throw std::out_of_range("index must be in [0, 3]");
    return data[index];
}

float& vec4::operator[](unsigned int index)
{
    if(index > 3 || index < 0) throw std::out_of_range("index must be in [0, 3]");
    return data[index];
}

vec4& vec4::operator=(const vec4 &v2)
{
    for(unsigned int i = 0; i <= 3; i++){
        data[i] = v2[i];
    }

    return *this;
}

bool vec4::operator==(const vec4 &v2) const
{
    return abs(data[0]-v2[0]) <= FLT_EPSILON
        && abs(data[1]-v2[1]) <= FLT_EPSILON
        && abs(data[2]-v2[2]) <= FLT_EPSILON
        && abs(data[3]-v2[3]) <= FLT_EPSILON;
}

bool vec4::operator!=(const vec4 &v2) const
{
    return !(*this == v2);
}

vec4& vec4::operator+=(const vec4 &v2)
{
    *this = *this + v2;

    return *this;
}

vec4& vec4::operator-=(const vec4 &v2)
{
    *this = *this - v2;

    return *this;
}

vec4& vec4::operator*=(float c)
{
    *this = *this * c;

    return *this;
}

vec4& vec4::operator/=(float c)
{
    *this = *this / c;

    return *this;
}

vec4 vec4::operator+(const vec4 &v2) const
{
    return vec4(data[0]+v2[0], data[1]+v2[1], data[2]+v2[2], data[3]+v2[3]);
}

vec4 vec4::operator-(const vec4 &v2) const
{
    return vec4(data[0]-v2[0], data[1]-v2[1], data[2]-v2[2], data[3]-v2[3]);
}

vec4 vec4::operator*(float c) const
{
    return vec4(c*data[0], c*data[1], c*data[2], c*data[3]);
}

vec4 vec4::operator/(float c) const
{
    if(fabs(c - 0) <= FLT_EPSILON) throw std::invalid_argument("divisor can't be 0");
    return vec4(data[0]/c, data[1]/c, data[2]/c, data[3]/c);
}

float dot(const vec4 &v1, const vec4 &v2)
{
    float sum = 0;

    for(unsigned int i = 0; i <= 3; i++){
        sum += v1[i] * v2[i];
    }

    return sum;
}

vec4 cross(const vec4 &v1, const vec4 &v2)
{
    return vec4(v1[1]*v2[2] - v2[1]*v1[2],
                    v1[2]*v2[0] - v2[2]*v1[0],
                    v1[0]*v2[1] - v2[0]*v1[1],
                    v1[3]*v2[3]);
}

float length(const vec4 &v)
{
    return sqrt(pow(v[0], 2.0) + pow(v[1], 2.0) + pow(v[2], 2.0) + pow(v[3], 2.0));
}

vec4 operator*(float c, const vec4 &v)
{
    return vec4(c*v[0], c*v[1], c*v[2], c*v[3]);
}

vec4 normalize(const vec4 &v)
{
    return vec4(v) / length(v);
}

std::ostream& operator<<(std::ostream &o, const vec4 &v)
{
    o << "[" << v[0] << "\n " << v[1]
      << "\n " << v[2] << "\n " << v[3] << "]";
    return o;
}
