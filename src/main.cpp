// Base code written by Jan Allbeck, Chris Czyzewicz, Cory Boatright, Tiantian Liu, Benedict Brown, and Adam Mally
// University of Pennsylvania

// At least some C++ compilers do funny things
// to C's math.h header if you don't define this
#define _USE_MATH_DEFINES

#include "vec4.h"
#include "mat4.h"
#include <iostream>
#include <math.h>
#include <typeinfo>
#include <stdexcept>
#include "assert.h"

using namespace std;

int main() {
    //vec4 test
    cout<<"******************** vec4 test ********************"<<endl;

    //test vec4::vec4() and std::ostream& operator<<(std::ostream &o, const vec4 &v)
    const vec4 v1 = vec4();
    assert(v1[0] == 0 && v1[1] == 0 && v1[2] == 0 && v1[3] == 0);
    cout<<"v1"<<"\n"<<v1<<"\n"
        <<"vec4::vec4() test passed"<<"\n"
        <<"std::ostream& operator<<(std::ostream &o, const vec4 &v) test passed"<<endl;

    //test float vec4::operator[](unsigned int index) const
    try{
        v1[4];
    }catch(std::out_of_range ex){
        cout<<"float vec4::operator[](unsigned int index) const test passed"<<endl;
    }

    //test vec4::vec4(const vec4 &v2)
    vec4 v2 = vec4(v1);
    assert(v2[0] == 0 && v2[1] == 0 && v2[2] == 0 && v2[3] == 0);
    cout<<"v2 "<<"\n"<<v2<<"\n"
        <<"vec4::vec4(const vec4 &v2) test passed"<<endl;

    //test bool vec4::operator==(const vec4 &v2) const
    assert(v2 == v1);
    cout<<"bool vec4::operator==(const vec4 &v2) const test passed"<<endl;

    //test float& vec4::operator[](unsigned int index)
    try{
        v2[4];
    }catch(std::out_of_range ex){
        cout<<"float& vec4::operator[](unsigned int index) test passed"<<endl;
    }

    //test vec4::vec4(float x, float y, float z, float w) and vec4& vec4::operator=(const vec4 &v2)
    v2 = vec4(2, 4, 6, 1);
    assert(v2[0] == 2 && v2[1] == 4 && v2[2] == 6 && v2[3] == 1);
    cout<<"v2 "<<"\n"<<v2<<"\n"
       <<"vec4::vec4(float x, float y, float z, float w) test passed"<<endl;

    //test bool vec4::operator!=(const vec4 &v2) const
    assert(v2 != v1);
    cout<<"bool vec4::operator!=(const vec4 &v2) const test passed"<<endl;

    //test vec4& vec4::operator=(const vec4 &v2)
    vec4& v3 = v2;
    assert(v3 == v2);
    cout<<"v3 "<<"\n"<<v3<<"\n"
        <<"vec4& vec4::operator=(const vec4 &v2) test passed"<<endl;

    //test vec4 vec4::operator+(const vec4 &v2) const
    assert(v3 + v2 == vec4(4, 8, 12, 2));
    cout<<"vec4 vec4::operator+(const vec4 &v2) const test passed"<<endl;

    //test vec4 vec4::operator-(const vec4 &v2) const
    assert(v3 - v2 == vec4());
    cout<<"vec4 vec4::operator-(const vec4 &v2) const test passed"<<endl;

    //test vec4 vec4::operator*(float c) const
    assert(v3 * 3 == vec4(6, 12, 18, 3));
    cout<<"vec4 vec4::operator*(float c) const test passed"<<endl;

    //test vec4 vec4::operator/(float c) const
    assert(v3 / 2 == vec4(1, 2, 3, 0.5));
    try{
        v3 / 0;
    }catch(std::invalid_argument ex){
        cout<<"vec4 vec4::operator/(float c) const test passed"<<endl;
    }

    //test vec4& vec4::operator+=(const vec4 &v2)
    v3 += v2;
    assert(v3 == vec4(4, 8, 12, 2));
    cout<<"vec4& vec4::operator+=(const vec4 &v2) test passed"<<endl;

    //test vec4& vec4::operator-=(const vec4 &v2)
    v3 -= v2;
    assert(v3 == vec4());
    cout<<"vec4& vec4::operator-=(const vec4 &v2) test passed"<<endl;

    //test vec4& vec4::operator*=(float c)
    v3 = vec4(2, 4, 6, 1);
    v3 *= 2;
    assert(v3 == vec4(4, 8, 12, 2));
    cout<<"vec4& vec4::operator*=(float c) test passed"<<endl;

    //test vec4& vec4::operator/=(float c)
    v3 /= 2;
    assert(v3 == vec4(2, 4, 6, 1));
    cout<<"vec4& vec4::operator/=(float c) test passed"<<endl;

    //test float dot(const vec4 &v1, const vec4 &v2)
    float dotRes = dot(v3, vec4(0.5, 0.5, 0.5, 1));
    assert(dotRes == 7.0);
    cout<<"float dot(const vec4 &v1, const vec4 &v2) test passed"<<endl;

    //test vec4 cross(const vec4 &v1, const vec4 &v2)
    assert(cross(v3, vec4(1, 1, 1, 10)) == vec4(-2, 4, -2, 10));
    cout<<"vec4 cross(const vec4 &v1, const vec4 &v2) test passed"<<endl;

    //test float length(const vec4 &v)
    assert(length(vec4(1, 1, 1, 1)) == (float)2);
    cout<<"float length(const vec4 &v) test passed"<<endl;

    //test vec4 operator*(float c, const vec4 &v)
    assert(2 * v3 == vec4(4, 8, 12, 2));
    cout<<"vec4 operator*(float c, const vec4 &v) test passed"<<endl;

    //test vec4 normalize(const vec4 &v)
    assert(normalize(v3) == v3 / length(v3));
    cout<<"vec4 normalize(const vec4 &v) test passed"<<endl;

    //mat4 test
    cout<<"\n"<<"******************** mat4 test ********************"<<endl;

    //test mat4::mat4() and std::ostream& operator<<(std::ostream& o, const mat4 &m)
    const mat4 m1 = mat4();
    assert(m1[0] == vec4() && m1[1] == vec4() && m1[2] == vec4() && m1[3] == vec4());
    cout<<"m1"<<"\n"<<m1<<"\n"
        <<"mat4::mat4() test passed"<<"\n"
        <<"std::ostream& operator<<(std::ostream& o, const mat4 &m) test passed"<<endl;

    //test vec4 mat4::operator[](unsigned int index) const
    try{
        m1[4];
    }catch(std::out_of_range ex){
        cout<<"vec4 mat4::operator[](unsigned int index) const test passed"<<endl;
    }

    //test mat4::mat4(const mat4 &m2)
    mat4 m2 = mat4(m1);
    assert(m2[0] == vec4() && m2[1] == vec4() && m2[2] == vec4() && m2[3] == vec4());
    cout<<"m2"<<"\n"<<m2<<"\n"
        <<"mat4::mat4(const mat4 &m2) test passed"<<endl;

    //test bool mat4::operator==(const mat4 &m2) const
    assert(m2 == m1);
    cout<<"bool mat4::operator==(const mat4 &m2) const test passed"<<endl;

    //test vec4& mat4::operator[](unsigned int index)
    try{
        m2[4];
    }catch(std::out_of_range ex){
        cout<<"vec4& mat4::operator[](unsigned int index) test passed"<<endl;
    }

    //test mat4::mat4(const vec4& col0, const vec4& col1, const vec4& col2, const vec4& col3)
    m2 = mat4(vec4(2, 2, 2, 2), vec4(4, 4, 4, 4),
              vec4(6, 6, 6, 6), vec4(1, 1, 1, 1));
    assert(m2[0] == vec4(2, 2, 2, 2) && m2[1] == vec4(4, 4, 4, 4)
        && m2[2] == vec4(6, 6, 6, 6) && m2[3] == vec4(1, 1, 1, 1));
    cout<<"m2"<<"\n"<<m2<<"\n"
        <<"mat4::mat4(const vec4& col0, const vec4& col1, const vec4& col2, const vec4& col3) test passed"<<endl;

    //test mat4::mat4(float diag)
    mat4 m3 = mat4(1);
    assert(m3[0] == vec4(1, 0, 0, 0) && m3[1] == vec4(0, 1, 0, 0)
        && m3[2] == vec4(0, 0, 1, 0) && m3[3] == vec4(0, 0, 0, 1));
    cout<<"m3"<<"\n"<<m3<<"\n"
        <<"mat4::mat4(float diag) test passed"<<endl;

    //test bool mat4::operator !=(const mat4 &m2) const
    assert(m3 != m2);
    cout<<"bool mat4::operator !=(const mat4 &m2) const test passed"<<endl;

    //test static mat4 mat4::identity()
    assert(m3 == mat4::identity());
    cout<<"static mat4 mat4::identity() test passed"<<endl;

    //test static mat4 mat4::rotate(float angle, float x, float y, float z)
    assert(mat4::rotate(90, 1, 0 ,0) == mat4(vec4(1, 0, 0, 0), vec4(0, 0, 1, 0),
                                            vec4(0, -1, 0, 0), vec4(0, 0, 0 ,1)));
    cout<<"mat4::rotate(90, 1, 0 ,0)"<<"\n"<<mat4::rotate(90, 1, 0 ,0)<<"\n";
    assert(mat4::rotate(90, 0, 1 ,0) == mat4(vec4(0, 0, -1, 0), vec4(0, 1, 0, 0),
                                            vec4(1, 0, 0, 0), vec4(0, 0, 0 ,1)));
    cout<<"mat4::rotate(90, 0, 1 ,0)"<<"\n"<<mat4::rotate(90, 0, 1 ,0)<<"\n";
    assert(mat4::rotate(90, 0, 0 ,1) == mat4(vec4(0, 1, 0, 0), vec4(-1, 0, 0, 0),
                                            vec4(0, 0, 1, 0), vec4(0, 0, 0 ,1)));
    cout<<"mat4::rotate(90, 0, 0 ,1)"<<"\n"<<mat4::rotate(90, 0, 0 ,1)<<"\n";
    try{
        mat4::rotate(90, 1, 1, 1);
    }catch(std::invalid_argument ex){
        cout<<"static mat4 mat4::rotate(float angle, float x, float y, float z) test passed"<<endl;
    }

    //test static mat4 mat4::translate(float x, float y, float z)
    assert(mat4::translate(1, 2, 3) == mat4(vec4(1, 0, 0, 0), vec4(0, 1, 0, 0),
                                            vec4(0, 0, 1, 0), vec4(1, 2, 3 ,1)));
    cout<<"mat4::translate(1, 2, 3)"<<"\n"<<mat4::translate(1, 2, 3)<<"\n"
        <<"static mat4 mat4::translate(float x, float y, float z) test passed"<<endl;

    //test static mat4 mat4::scale(float x, float y, float z)
    assert(mat4::scale(1, 2, 3) == mat4(vec4(1, 0, 0, 0), vec4(0, 2, 0, 0),
                                        vec4(0, 0, 3, 0), vec4(0, 0, 0 ,1)));
    cout<<"mat4::scale(1, 2, 3)"<<"\n"<<mat4::scale(1, 2, 3)<<"\n"
        <<"static mat4 mat4::scale(float x, float y, float z) test passed"<<endl;

    //test mat4& mat4::operator=(const mat4 &m2)
    mat4& m4 = m2;
    assert(m4 == m2);
    cout<<"mat4& mat4::operator=(const mat4 &m2) test passed"<<endl;

    //test mat4 mat4::operator+(const mat4 &m2) const
    assert(mat4() + mat4::identity() == mat4::identity());
    cout<<"mat4 mat4::operator+(const mat4 &m2) const test passed"<<endl;

    //test mat4 mat4::operator-(const mat4 &m2) const
    assert(mat4::identity() - mat4::identity() == mat4());
    cout<<"mat4 mat4::operator-(const mat4 &m2) const test passed"<<endl;

    //test mat4 mat4::operator*(float c) const
    assert(mat4::identity() * 2 == mat4::identity() + mat4::identity());
    cout<<"mat4 mat4::operator*(float c) const test passed"<<endl;

    //test mat4 mat4::operator/(float c) const
    assert(mat4::identity() / (float)0.5 == mat4::identity() + mat4::identity());
    cout<<"mat4 mat4::operator/(float c) const test passed"<<endl;

    //test mat4& mat4::operator+=(const mat4 &m2)
    m3 += mat4::identity();
    assert(m3 == mat4::identity() * 2);
    cout<<"mat4& mat4::operator+=(const mat4 &m2) test passed"<<endl;

    //test mat4& mat4::operator-=(const mat4 &m2)
    m3 -= mat4::identity();
    assert(m3 == mat4::identity());
    cout<<"mat4& mat4::operator-=(const mat4 &m2) test passed"<<endl;

    //test mat4& mat4::operator*=(float c)
    m3 *= 2;
    assert(m3 == mat4::identity() * 2);
    cout<<"mat4& mat4::operator*=(float c) test passed"<<endl;

    //test mat4& mat4::operator/=(float c)
    m3 /= 2;
    assert(m3 == mat4::identity());
    cout<<"mat4& mat4::operator/=(float c) test passed"<<endl;

    //test mat4 mat4::operator*(const mat4 &m2) const
    mat4 m5 = mat4(vec4(2, 4, 6, 1), vec4(2, 4, 6, 1),
                   vec4(2, 4, 6, 1), vec4(2, 4, 6, 1));
    assert(m5 * mat4::scale(4, 3, 2) == mat4(vec4(8, 16, 24, 4), vec4(6, 12, 18, 3),
                                             vec4(4, 8, 12, 2), vec4(2, 4, 6, 1)));
    cout<<"mat4 mat4::operator*(const mat4 &m2) const test passed"<<endl;

    //test vec4 mat4::operator*(const vec4 &v) const
    assert(m5 * vec4(1, 1, 1, 1) == vec4(8, 16, 24, 4));
    cout<<"vec4 mat4::operator*(const vec4 &v) const test passed"<<endl;

    //test mat4 transpose(const mat4 &m)
    assert(transpose(m5) == m2);
    cout<<"mat4 transpose(const mat4 &m) test passed"<<endl;

    //test vec4 row(const mat4 &m, unsigned int index)
    assert(row(m5, 1) == m2[1]);
    try{
        row(m5, 4);
    }catch(std::out_of_range ex){
        cout<<"vec4 row(const mat4 &m, unsigned int index) test passed"<<endl;
    }

    //test mat4 operator*(float c, const mat4 &m)
    assert(2 * m5 == m5 * 2);
    cout<<"mat4 operator*(float c, const mat4 &m) test passed"<<endl;

    //test vec4 operator*(const vec4 &v, const mat4 &m)
    assert(vec4(1, 1, 1, 1) * m5 == vec4(2, 2, 2, 2));
    cout<<"vec4 operator*(const vec4 &v, const mat4 &m) test passed"<<endl;
}
