TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    src/main.cpp \
    src/vec4.cpp \
    src/mat4.cpp

HEADERS += \
    src/mat4.h \
    src/vec4.h
